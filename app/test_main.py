from fastapi.testclient import TestClient

from .main import app

test = TestClient(app)


def test_health():
    response = test.get("/-/health")
    assert response.status_code == 200
    assert response.json()

def test_api():
    response = test.get("/api/echo?text=foo")
    assert response.status_code == 200
    assert response.json() == {"text": "foo"}
