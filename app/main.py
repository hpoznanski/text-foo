from fastapi import FastAPI, Response, status
import platform

app = FastAPI()

@app.get("/-/health")
def healthcheck():
    return platform.uname()

@app.get("/api/echo")
def api_echo(response: Response, text: str = None):
    if not text:
        response.status_code = status.HTTP_400_BAD_REQUEST
        return {"Response": "Wrong API parameter"}
    response.status_code = status.HTTP_200_OK
    return {"text": text}
