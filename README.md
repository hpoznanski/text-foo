# text-foo

  * [Application](#application)
  * [Server config](#server-config)
  * [CI/CD](#ci-cd)
  * [Helm Chart / Kubernetes deployment](#helm-chart---kubernetes-deployment)
  * [Infrastructure as code](#infrastructure-as-code)

## Application

A simple application [`/app/main.py`](https://gitlab.com/hpoznanski/text-foo/-/blob/main/app/main.py) written using FastAPI serving two endpoints.
*  GET /-/health -> based on platform.uname it returns information about host
*  GET /-/api/echo?text=foo -> returns JSON object with key text and value passed to the param string i.e. `{ text = foo }` 

Why FastAPI? Because is a rather minimalistic framework, more of the likes of Flask (and its much faster ). Is built using modern Python concepts. Theres a couple of fancy features out-of-box like Automatic Docs (Swagger UI) Validation, Security and Auth.

## Server config 

In this case, [I used uvicorn with default configuration](https://gitlab.com/hpoznanski/text-foo/-/blob/main/Dockerfile#L11). Uvicorn is a high performance ASGI server. We could also use WSGI such as Gunicorn which is sunchronous callable. It all depends of the development and what we need in given case. For a larger solutions I would think about additional reverse proxy, loadbalancing and more complex server solution.

## CI/CD

For the purpose of this project, I used the standard [gitlab-ci](https://gitlab.com/hpoznanski/text-foo/-/blob/main/.gitlab-ci.yml) in which I included three stages:

* linter - based on python-linting / black test
* build - docker build, tagged artifact (docker image) is in gitlab registry of this project
* test - based on [newman](https://learning.postman.com/docs/running-collections/using-newman-cli/command-line-integration-with-newman/), the test configuration is in the collection.json file. Newman allows to easily define tests, reuse them in postman, present them in graphical form (i.e. export results to HTML or generate custom reports) Alternatively, we can use [`/app/test_main.py`](https://gitlab.com/hpoznanski/text-foo/-/blob/main/app/test_main.py) with pytest.

As a CD I would suggest Argo CD which is declarative continuous delivery tool for Kubernetes. It provide a automated, auditable and easy to understand application deployment and lifecycle managament.

## Helm Chart / Kubernetes deployment

I built a standard helm chart using `helm create` with some minor changes. The helm chart itself as well as the definitions of ingress for example, or autoscaling depends on the company's policy and what standards it takes and about what application we are talking about. Default I added  `security context` and `pod security context` also uncomment limits and requests resources. The definition of resources should be based on metrics e.g. from VPA recommender also perf tests would be useful for estimating the resources needed. Depending on the application, I would also use custom metrics from Prometheus. Also I would base the monitoring of application and Kubernetes cluster on a system of metrics (Prometheus).

## Infrastructure as code

I would suggest using Terraform as a IaaC tool. It is difficult to define even a code example without knowing the target infrastracture so I will provide examples of actions what I would do for optimal:
* creating Kubernetes cluster using offical module
* (Only Terraform Cloud) enable Cost Estimation. It provides cost estimates for many resources in Terraform code. When enabled, Terraform Cloud performs a cost estimate for every run.
* Deploy Finops tool like Kubecost for better cost visibility